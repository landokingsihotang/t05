/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 * "Jawab disini..." 
 * Jika ada beberapa operasi dasar yang perlu diimplementasikan oleh class child kalkulator,
 * dan terdapat implementasi umum yang dapat digunakan pada class child , penggunaan abstract class lebih cocok.
 * Karna abstract class dapat memberikan implementasi default (umum)
 * untuk operasi dasar tersebut sehingga kelas-kelas turunan tidak perlu mengulang implementasi yang sama,
 * bahkan dapat memperluas fungsionalitas method yang ada pada class parent.
 * Apabila terdapat method abstract pada class abstract ,maka method tersebut harus diimplementasikan
 * di setiap class child sehingga apabila kalkulator yang dibuat memiliki kesamaan ,maka method abstract ini 
 * akan sangat berguna.
 * Namun apabila method abstract ini dibuat pada abstract class
 * dan tidak banyak implemetasikan pada class child, maka hal ini membuat method abstract pada abstract class
 * menjadi tidak flexibel,dimana harus semua diimplementasikan pada child class padahal tidak digunakan
 * ""KESIMPULAN"""
 * Apabila kalkulator yang dibuat memiliki kesamaan dalam mengimplementasikan hal yang sama 
 * maka abstract class lebih cocok karena tidak perlu menulis ulang kode,namun boleh di perluas fungsionalitasnya.
 * Selain itu apabila method abstract pada class abstract memiliki hubungan hierarki dengan class-class konkret lainnya,
 * penggunaan class abstract dengan method abstract memang lebih sesuai digunakan pada kalkulator nantinya.
 * 
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operand1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operand2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        kalkulator.setOperan(operand1, operand2);
        double result = kalkulator.hitung();
        System.out.println("Hasil: " + result);
    }
}