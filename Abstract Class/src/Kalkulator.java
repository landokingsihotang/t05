abstract class Kalkulator {
    double operan1, operan2;
    //Do your magic here...
    //method abstract untuk diimplementasikan di class pengurangan dan penjumlahan
    public abstract void setOperan(double operand1, double operand2);//untuk mengoper nilai input dari pengguna
    public abstract double hitung();// untuk melakukan perhitungkan pada input pengguna
       
}