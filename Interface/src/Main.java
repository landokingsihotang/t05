/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Jawab disini..." 
 * Jika kalkulator yang dibuat hanya membutuhkan beberapa metode yang harus diimplementasikan oleh kelas-kelas konkret
 * seperti class pengurangan dan penjumlahan ini, maka penggunaan interface lebih cocok.
 * Dalam hal ini,
 * dalam class pengurangan hanya memiliki 1 method yaitu mengurangkan angka 
 * dan dalam class penjumlahan juga hanya memiliki method menambahkan angka.
 * selain itu interface cocok dan ditekankan digunakan untuk menambahkan method-method khusus
 * yang tidak memiliki hubungan hierarki dengan class-class konkret lainnya.
 * Sehingga secara keseluruhan,kalkulator sederhana cocok untuk menggunakan class interface saja.
 * karena tak memiliki komponen yang kompleks dalam class nya seperti penjumlahan dan pengurangan ini.
 * sehingga dengan interface,Kalkulator menjadi lebih fleksibel ,karena class konkret dapat mengimplementasikan 
 * implementasi khusus sesuai kebutuhan class .
 * ""KESIMPULAN"""
 * Apabila kalkulator yang dibuat memerlukan implementasi method yang khusus
 * dan hanya perlu dimiliki class tertentu saja, maka interface lebih baik digunakan.
 * 
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}